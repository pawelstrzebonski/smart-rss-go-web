# Smart-RSS Go Web

A machine learning enhanced RSS feed manager (written in Go). Unlike the parallel project [Smart-RSS Go](https://gitlab.com/pawelstrzebonski/smart-rss-go) (which has a native desktop UI), this application runs a local web-server that allows you to access the web-UI in your web-browser of choice. Both of these applications are built on the [`libsmartrssgo`](https://gitlab.com/pawelstrzebonski/libsmartrssgo) library.

For more information about program design/implementation, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/smart-rss-go-web/).

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript

## Screenshots

![Main web UI](docs/img/webui.png)

## Usage/Installation

This repository contains the Go source code in the top level. It can be built using the standard Go compiler and may require installation of build dependencies from your systems package respositories. A `Makefile` is included with most of the relevant build/install commands, although build dependency installation is operating system dependent and is not included.

If you have [Nix](https://nixos.org/) then you can use the included `shell.nix` to start a shell with installed dependencies for development, or else you can use the `default.nix` script to build/install a specific version of this application. Note that `default.nix` specifies the commit to be built, so it may need to be updated to build the latest version.

Note: At the moment, the HTML templates are not bundled into the binary, so it may need to run from a directory with the `templates` directory within it.

Once built, run the `smartrssgoweb` binary to start the back-end server. To access the UI, open in your browser of choice `localhost:8080` (this may be different depending on platform). The application takes command-line arguments `-port` (port number on which to serve this application), `-pure` (do not read/write to the database, use default template, primarily for debugging), `-db $DATABASEFILENAME` to define the database to create/use by filename, and `-template $TEMPLATEFILENAME` to specify the template file to load (useful for developing/customizing the template/UI).
