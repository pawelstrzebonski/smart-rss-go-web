# Smart-RSS Go Web

Smart-RSS Go Web is a machine learning enhanced RSS feed manager, written in Go, with a back-end web-server and web-UI front-end. There is a parallel project, [Smart-RSS Go](https://gitlab.com/pawelstrzebonski/smart-rss-go) which provides much of the same functionality, but in a native desktop application form. Both of these projects use [libsmartrssgo](https://gitlab.com/pawelstrzebonski/libsmartrssgo) as a backend for the RSS and machine learning functionality.

## Features

* Automatic periodic RSS feed checking
* Machine learning scoring and ordered presentation of feed items
* Local database and processing
* Web UI, use the web browser of your choice
* No JavaScript

## Screenshots

![Main web UI](img/webui.png)

![Scored words web page](img/wordscores.png)
