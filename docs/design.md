# Design Document

## Files

As of writing, there are three main files that comprise this application's source:

* `main.go`
* `templates/layout.tmpl`
* `templates/wordscores.tmpl`

### `main.go`

This is the Go source file.

First, it will setup the database/RSS/machine-learning back-end provided by the `libsmartrssgo` package. It will load or create the database for RSS feeds/items, update the feeds on start, and setup an automatic periodic check for updates.

Next, it will setup the router/web-server. This will enable a user to access the main UI in their web browser of choice (likely at a URL of `localhost:8080`). The custom router will work with the server provided by `net/http` create and server the web UI pages and handle user feedback (in the form of POST requests to the server), passing them along to the `libsmartrssgo` backend, as appropriate.

### `templates/layout.tmpl` and `templates/wordscores.tmpl`

These are the web UI HTML template files, used by `html/template`.

The `layout.tmpl` template will display a list of RSS items (with their titles, summaries, and `libsmartrssgo` assigned scores) and provide for each a set of buttons for opening the item in a new tab, "liking", and "disliking" the item (these are local (dis)likes used only by the `libsmartrssgo` backend for scoring items). The `wordscores.tmpl` template will display a table of words and their corresponding scores from the classifier.

To avoid JavaScript and unnecessary page reloads, all interaction with the backend is using POST commands. To avoid reloading the page each time a user (dis)likes an RSS item, a hidden iframe is included at the end of the document and will be updated instead.

At the bottom of the main page, the template provides forms for display and back-end settings (such as the # of feed items to show at the same time, and the option to add new RSS items by URL).
