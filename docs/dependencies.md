# Dependencies

This application makes use of the following Go packages:

* [`libsmartrssgo`](https://gitlab.com/pawelstrzebonski/libsmartrssgo): backend for RSS/database/machine-learning components
