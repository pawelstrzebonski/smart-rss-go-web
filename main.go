package main

import (
	"embed"
	"errors"
	"flag"
	"fmt"
	"gitlab.com/pawelstrzebonski/libsmartrssgo"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

// Return smaller of integers
func MinInt(a int, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

// If an error, log as fatal
func logErrFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Embed templates and assets (favicon) into binary as a filesystem
//
//go:embed assets/favicon.ico
var f embed.FS

//go:embed templates/layout.tmpl
var defaulttemplate []byte

//go:embed templates/wordscores.tmpl
var wordscorestemplate []byte

// Struct with data for rendering template of main page
type page struct {
	Items        []libsmartrssgo.MyItem
	ItemsPerPage string
	Feeds        []libsmartrssgo.MyFeed
}

// Struct with data for rendering template of word score page
type wpage struct {
	Words []libsmartrssgo.ScoredWord
}

func main() {
	// Get execution options from flags
	pure := flag.Bool("pure", false, "Enable pure/in-memory mode (does not access files, will lose all data on restart)")
	port := flag.Int("port", 8080, "Port on which to server application")
	limit := flag.Int("limit", 50, "Maximal number of feed items to show per page")
	dbfile := flag.String("db", "smartrssv1.gob", "Name of database file (within configuration directory)")
	PASSWORD := flag.String("password", "", "Password for authentication")
	ishttps := flag.Bool("https", false, "Enable HTTPS mode (using program-generated self-signed certificate)")
	flag.Parse()
	// Setup the filepath for the database
	homepath, err := os.UserHomeDir()
	logErrFatal(err)
	apppath := filepath.Join(homepath, ".config", "smartrss")
	if !*pure {
		err = os.MkdirAll(apppath, os.ModePerm)
		logErrFatal(err)
	}
	// Load and parse template from configuration directory
	templ := template.Must(template.New("layout.tmpl").Parse(string(defaulttemplate)))
	// Load the embedded template for word-scores page
	templ.New("wordscores.tmpl").Parse(string(wordscorestemplate))
	// Create/load the database
	dbpath := filepath.Join(apppath, *dbfile)
	if *pure {
		// Create in-memory template in pure mode
		dbpath = ""
	}
	log.Print("Setting up back-end")
	smartrss := libsmartrssgo.Setup(dbpath, libsmartrssgo.ERROR, true)
	// Defer closing/saving the database
	defer smartrss.Close()
	// Updated feeds on application start
	smartrss.FeedsUpdate()
	// Set automatic feed updating to run every 600 seconds
	smartrss.SetAutoUpdate(600)
	// Set automatic database saving run every 3600 seconds
	smartrss.SetAutoSave(3600)
	log.Print("Back-end set up")

	// Set initial # of items to display on a page
	numitemsperpage := *limit

	// Get first batch of unread items
	feeds := smartrss.FeedsList()
	items := smartrss.ItemsUnreadGetLimited(uint64(numitemsperpage))

	// Create own request router
	router := func(w http.ResponseWriter, r *http.Request) {
		// Get username (not checked) and password
		if len(*PASSWORD) > 0 {
			_, password, ok := r.BasicAuth()
			if !ok {
				w.Header().Add("WWW-Authenticate", `Basic realm="Give username and password"`)
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(`{"message": "No basic auth present"}`))
				return
			}
			// Compare password from browser to password from launch arguments
			if password != *PASSWORD {
				w.Header().Add("WWW-Authenticate", `Basic realm="Give username and password"`)
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte(`{"message": "Invalid username or password"}`))
				return
			}
		}
		// Create appropriate response based on path and request method
		switch r.URL.Path {
		case "/favicon.ico":
			if r.Method == "GET" {
				p, err := f.ReadFile("assets/favicon.ico")
				logErrFatal(err)
				log.Print("Serving favicon")
				w.Write(p)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/":
			if r.Method == "GET" {
				n := MinInt(len(items), numitemsperpage)
				p := page{items[0:n],
					strconv.Itoa(numitemsperpage),
					feeds,
				}
				err := templ.ExecuteTemplate(w, "layout.tmpl", p)
				logErrFatal(err)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/wordscores":
			if r.Method == "GET" {
				p := wpage{smartrss.AllWordScoresGet()}
				err := templ.ExecuteTemplate(w, "wordscores.tmpl", p)
				logErrFatal(err)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/add":
			if r.Method == "POST" {
				if err := r.ParseForm(); err != nil {
					fmt.Fprintf(w, "ParseForm() err: %v", err)
					return
				}
				url := r.FormValue("url")
				num, _ := strconv.Atoi(r.FormValue("num"))
				log.Print("Adding feed ", url, " with update period of ", num)
				smartrss.FeedAdd(url, uint64(num))
				feeds = smartrss.FeedsList()
				http.Redirect(w, r, "/", http.StatusFound)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/remove":
			if r.Method == "POST" {
				if err := r.ParseForm(); err != nil {
					fmt.Fprintf(w, "ParseForm() err: %v", err)
					return
				}
				url := r.FormValue("url")
				log.Print("Removing feed ", url)
				smartrss.FeedRemove(url)
				feeds = smartrss.FeedsList()
				http.Redirect(w, r, "/", http.StatusFound)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/nextpage":
			if r.Method == "POST" {
				log.Print("Showing next set of items...")
				// Mark shown items as viewed in database
				n := MinInt(len(items), numitemsperpage)
				for i := 0; i < n; i++ {
					smartrss.ItemMarkViewed(items[i].Url)
				}
				// Replace with next set of unread items
				items = smartrss.ItemsUnreadGetLimited(uint64(numitemsperpage))
				// Redirect to home page (i.e. refresh)
				http.Redirect(w, r, "/", http.StatusFound)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/like":
			if r.Method == "POST" {
				if err := r.ParseForm(); err != nil {
					fmt.Fprintf(w, "ParseForm() err: %v", err)
					return
				}
				url := r.FormValue("url")
				log.Print("Liking ", url)
				smartrss.ItemLike(url)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/dislike":
			if r.Method == "POST" {
				if err := r.ParseForm(); err != nil {
					fmt.Fprintf(w, "ParseForm() err: %v", err)
					return
				}
				url := r.FormValue("url")
				log.Print("Disliking ", url)
				smartrss.ItemDislike(url)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/open":
			if r.Method == "POST" {
				if err := r.ParseForm(); err != nil {
					fmt.Fprintf(w, "ParseForm() err: %v", err)
					return
				}
				url := r.FormValue("url")
				log.Print("Opening ", url)
				smartrss.ItemMarkOpened(url)
				http.Redirect(w, r, url, http.StatusFound)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		case "/setitemnum":
			if r.Method == "POST" {
				num, _ := strconv.Atoi(r.FormValue("num"))
				numitemsperpage = num
				log.Print("Setting # of items per page to ", num)
				// Update with correct number of items
				items = smartrss.ItemsUnreadGetLimited(uint64(numitemsperpage))
				http.Redirect(w, r, "/", http.StatusFound)
			} else {
				http.Error(w, "405 method not allowed.", http.StatusMethodNotAllowed)
			}
		default:
			//Default on bad path
			http.Error(w, "404 not found.", http.StatusNotFound)
		}
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", router)
	// Launch the web server
	if *ishttps {
		certfile, keyfile := filepath.Join(apppath, "cert.pem"), filepath.Join(apppath, "key.pem")
		// Check if certificate files already exist
		_, err = os.Stat(certfile)
		// If not, generate them
		if errors.Is(err, os.ErrNotExist) {
			keygen(fmt.Sprintf(":%d", *port), apppath)
		}
		http.ListenAndServeTLS(fmt.Sprintf(":%d", *port), certfile, keyfile, mux)
	} else {
		http.ListenAndServe(fmt.Sprintf(":%d", *port), mux)
	}
}
