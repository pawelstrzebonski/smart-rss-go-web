{ pkgs ? import <nixpkgs> {} }:
with pkgs;

buildGoModule rec {
  name = "smartrssgoweb";
  buildInputs = [
	go
  ];
  vendorHash = "sha256-TM5c0PBOC3wJWi8C7ndta1s7ABm+K5BcLGWCjhGzkj8=";
  rev = "d6ff7bb7ed78d0392a64a34fbcee2cfc5a5058ed";
  src = fetchFromGitLab {
    inherit rev;
    owner = "pawelstrzebonski";
    repo = "smart-rss-go-web";
    sha256 = "sha256-L6B49ZsfgBrLwrwULH5Fil0BaL+XxMRQEewL6MwLx80=";
  };
  meta = with lib; {
    description = "A machine learning enhanced RSS feed manager with a web-server back-end and a web-browser front-end, written in Go.";
    homepage = "https://gitlab.com/pawelstrzebonski/smart-rss-go-web";
    license = licenses.agpl3Plus;
    #maintainers = with maintainers; [ pawelstrzebonski ];
    platforms = platforms.linux;
  };
}
